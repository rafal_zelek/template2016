package com.gft.digitalbank.exchange.solution

import akka.actor.{Actor, ActorLogging, Props}
import com.gft.digitalbank.exchange.solution.model.OrderMessageType

class LogicResultActor(product: String) extends Actor with ActorLogging {
  var previousLogicResult = LogicResult(List(), List(), List())

  override def receive: Receive = {
    case message: OrderMessageType =>
      log.debug(s"PRODUCT[$product] received $message for processing")
      previousLogicResult = Logic.processReceivedMessage(message, previousLogicResult)

    case GetResult =>
      log.debug(s"PRODUCT[$product] received GetResult")
      sender() ! LogicResultResponse(product, previousLogicResult)
    case any =>
      log.debug(s"PRODUCT[$product] Unhandled message: " + any)
  }
}

object LogicResultActor {
  def props(product: String) = Props(new LogicResultActor(product))
}

case object GetResult

case class LogicResultResponse(product: String, logicResult: LogicResult)