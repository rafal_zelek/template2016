package com.gft.digitalbank.exchange

import javax.jms.{Message, MessageListener}

package object solution {
  implicit def messageListenerDecorator(f: (Message) => Unit): MessageListener = new MessageListener {
    override def onMessage(message: Message): Unit = f(message)
  }
}
