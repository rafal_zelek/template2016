package com.gft.digitalbank.exchange.solution

import com.gft.digitalbank.exchange.solution.model._

case class LogicResult(transactions: List[Transaction], buyOrders: List[CreateBuyOrder], sellOrders: List[CreateSellOrder])


object Logic {

  private[solution] def processTransactions(logicResult: LogicResult) = {
    def sortBuyOrders(buyOrders: List[CreateBuyOrder]) = buyOrders.sortBy(r => (r.details.price, r.timestamp))(Ordering.Tuple2(Ordering.Int.reverse, Ordering.Long))
    def sortSellOrders(sellOrders: List[CreateSellOrder]) = sellOrders.sortBy(r => (r.details.price, r.timestamp))(Ordering.Tuple2(Ordering.Int, Ordering.Long))

    val sortedBuyOrders = sortBuyOrders(logicResult.buyOrders)
    val sortedSellOrders = sortSellOrders(logicResult.sellOrders)

    @scala.annotation.tailrec
    def helper(logicResult: LogicResult): LogicResult = {
      (logicResult.buyOrders, logicResult.sellOrders) match {
        case (buyOrder :: buyTail, sellOrder :: sellTail) if isTransactionPossible(buyOrder, sellOrder) =>
          val transactions: List[Transaction] = logicResult.transactions

          val transactionAmount = getTransactionAmount(buyOrder, sellOrder)
          val transactionId = getTransactionId(transactions)
          val transactionPrice = getTransactionPrice(buyOrder, sellOrder)

          val doneTransaction = Transaction(transactionId, Details(transactionAmount, transactionPrice), sellOrder.product, buyOrder.broker, sellOrder.broker, buyOrder.client, sellOrder.client)

          val listOfBuyOrder = {
            val restOfBuyOrder = buyOrder.copy(details = buyOrder.details.copy(amount = buyOrder.details.amount - transactionAmount))
            if (restOfBuyOrder.details.amount > 0) restOfBuyOrder :: buyTail else buyTail
          }

          val listOfSellOrder = {
            val restOfSellOrder = sellOrder.copy(details = sellOrder.details.copy(amount = sellOrder.details.amount - transactionAmount))
            if (restOfSellOrder.details.amount > 0) restOfSellOrder :: sellTail else sellTail
          }

          helper(LogicResult(doneTransaction :: transactions, listOfBuyOrder, listOfSellOrder))
        case _ => logicResult
      }
    }
    helper(logicResult.copy(buyOrders = sortedBuyOrders, sellOrders = sortedSellOrders))
  }

  private def getTransactionPrice(buyOrder: CreateBuyOrder, sellOrder: CreateSellOrder): Int = {
    if (sellOrder.timestamp < buyOrder.timestamp)
      sellOrder.details.price
    else
      buyOrder.details.price
  }

  private def getTransactionId(transactions: List[Transaction]): Int = {
    val by: List[Transaction] = transactions.sortBy(_.id).reverse
    val headOption: Option[Transaction] = by.headOption
    val map: Option[Int] = headOption.map(_.id).map(_ + 1)
    map.getOrElse(1)
  }

  private def getTransactionAmount(buyOrder: CreateBuyOrder, sellOrder: CreateSellOrder): Int = {
    Math.min(buyOrder.details.amount, sellOrder.details.amount)
  }

  private def isTransactionPossible(buyOrder: CreateBuyOrder, sellOrder: CreateSellOrder) = sellOrder.details.price <= buyOrder.details.price


  def processReceivedMessage(message: OrderMessageType, previousLogicResult: LogicResult): LogicResult = message match {
    case sellOrder: CreateSellOrder =>
      val newLogicResult = previousLogicResult.copy(sellOrders = previousLogicResult.sellOrders :+ sellOrder)
      processTransactions(newLogicResult)

    case buyOrder: CreateBuyOrder =>
      val newLogicResult = previousLogicResult.copy(buyOrders = previousLogicResult.buyOrders :+ buyOrder)
      processTransactions(newLogicResult)

    case modificateOrder: ModificateOrder if previousLogicResult.sellOrders.exists(_.id == modificateOrder.modifiedOrderId) =>
      val newLogicResult = previousLogicResult.copy(sellOrders = previousLogicResult.sellOrders.map { m =>
        if (m.id == modificateOrder.modifiedOrderId)
          m.copy(
            broker = modificateOrder.broker,
            details = modificateOrder.details,
            timestamp = modificateOrder.timestamp
          )
        else m
      })
      processTransactions(newLogicResult)

    case modificateOrder: ModificateOrder if previousLogicResult.buyOrders.exists(_.id == modificateOrder.modifiedOrderId) =>
      val newLogicResult = previousLogicResult.copy(buyOrders = previousLogicResult.buyOrders.map { m =>
        if (m.id == modificateOrder.modifiedOrderId)
          m.copy(
            broker = modificateOrder.broker,
            details = modificateOrder.details,
            timestamp = modificateOrder.timestamp
          )
        else m
      })
      processTransactions(newLogicResult)
    case cancelOrder: CancelOrder =>
      previousLogicResult.copy(sellOrders = previousLogicResult.sellOrders.filterNot(_.id == cancelOrder.cancelledOrderId),
        buyOrders = previousLogicResult.buyOrders.filterNot(_.id == cancelOrder.cancelledOrderId))
    case any =>
      previousLogicResult
  }
}
