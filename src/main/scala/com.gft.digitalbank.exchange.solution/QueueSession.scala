package com.gft.digitalbank.exchange.solution

import javax.jms.{ConnectionFactory, Session}
import javax.naming.InitialContext

import akka.actor.{Actor, ActorLogging}

trait QueueSession{
  self:Actor with ActorLogging =>
  private val connection = {
    val initialContext = new InitialContext()
    val connectionFactory = initialContext.lookup("ConnectionFactory").asInstanceOf[ConnectionFactory]
    val connection = connectionFactory.createConnection()
    log.debug("connection created")
    connection.start()
    log.debug("connection started")
    connection
  }
  val session = {
    log.debug("session creating")
    connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
  }

  override def postStop(): Unit = {
    log.debug("connection closed")
    session.close()
    connection.stop()
  }
}