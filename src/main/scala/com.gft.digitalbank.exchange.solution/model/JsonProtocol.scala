package com.gft.digitalbank.exchange.solution.model

import spray.json.DefaultJsonProtocol._
import spray.json.{JsObject, JsValue, JsonReader}


object JsonProtocol {
  implicit val details = jsonFormat2(Details)

  implicit object createSellOrder extends JsonReader[CreateSellOrder]{

    override def read(json: JsValue): CreateSellOrder = json match {
      case JsObject(map) if map.get("side").exists(_.toString == "\"SELL\"") => jsonFormat6(CreateSellOrder).read(json)
    }
  }

  implicit object createBuyOrder extends JsonReader[CreateBuyOrder]{
    override def read(json: JsValue): CreateBuyOrder = json match {
      case JsObject(map) if map.get("side").exists(_.toString == "\"BUY\"") => jsonFormat6(CreateBuyOrder).read(json)
    }
  }

  implicit val modificateOrder = jsonFormat5(ModificateOrder)
  implicit val cancelOrder = jsonFormat4(CancelOrder)
  implicit val shutdownNotification = jsonFormat0(ShutdownNotification)
}

/*
{
  "messageType": "order",
  "side": "BUY",
  "id": 1,
  "timestamp": 1,
  "broker": "b001",
  "client": "c001",
  "product": "abc",
  "details": {
    "amount": 10,
    "price": 1
  }
}
 */