package com.gft.digitalbank.exchange.solution.model

import spray.json.{JsonParser, JsonReader}

import scala.util.Try

trait MessageToFunctionConverter {

  val jsonToProperMessage: ConvertAndRun

  type ConvertAndRun = List[(String) => (Try[OrderMessageType], () => Unit)]

  def convertAndRun[A <: OrderMessageType : JsonReader](f: A => Unit)(jsonPar: String) = {
    val tried = Try(JsonParser(jsonPar).convertTo[A])
    (tried, () => tried.foreach(f))
  }

  def mapJsonToProperMessage(json: String): Option[() => Unit] = {
    val converted = jsonToProperMessage.view.map(f => f(json)).filter(_._1.isSuccess)
    val head = converted.head
    head._1.toOption.map(_ => head._2)
  }
}
