package com.gft.digitalbank.exchange.solution.model

sealed trait OrderMessageType

sealed trait WithTimestamp {
  val timestamp: Long
}

sealed trait WithProduct {
  val product: String
}


case class CreateSellOrder(id: Int,
                           timestamp: Long,
                           broker: String,
                           client: String,
                           product: String,
                           details: Details) extends OrderMessageType with WithProduct with WithTimestamp

case class CreateBuyOrder(id: Int,
                          timestamp: Long,
                          broker: String,
                          client: String,
                          product: String,
                          details: Details) extends OrderMessageType with WithProduct with WithTimestamp

case class ModificateOrder(id: Int,
                           timestamp: Long,
                           broker: String,
                           modifiedOrderId: Int,
                           details: Details) extends OrderMessageType with WithTimestamp

case class CancelOrder(id: Int,
                       timestamp: Long,
                       broker: String,
                       cancelledOrderId: Int) extends OrderMessageType with WithTimestamp

case class Details(amount: Int, price: Int)

case class ShutdownNotification() extends OrderMessageType

case class Transaction(id: Int, details: Details, product: String, brokerBuy: String, brokerSell: String, clientBuy: String, clientSell: String)