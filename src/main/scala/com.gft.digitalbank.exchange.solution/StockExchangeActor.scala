package com.gft.digitalbank.exchange.solution

import javax.jms.{Message, MessageListener, TextMessage}

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import akka.event.LoggingReceive
import akka.pattern.ask
import akka.util.Timeout
import com.gft.digitalbank.exchange.listener.ProcessingListener
import com.gft.digitalbank.exchange.model.{OrderBook, OrderEntry, SolutionResult, Transaction => JavaTransaction}
import com.gft.digitalbank.exchange.solution.StockExchangeActor.{RegisterBrockerQueue, SetupProcessingListener}
import com.gft.digitalbank.exchange.solution.model.JsonProtocol._
import com.gft.digitalbank.exchange.solution.model._

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try

class StockExchangeActor extends Actor with ActorLogging with QueueSession with MessageToFunctionConverter {


  var processingLogicActors: Map[String, ActorRef] = Map[String, ActorRef]()

  var processingListener: ProcessingListener = null

  var registeredNumber = 0

  var queue: List[(Long, WithTimestamp)] = List()

  val tickTime = 10

  val tick = context.system.scheduler.schedule(tickTime millis, tickTime millis, self, "tick")

  override def receive: Receive = LoggingReceive {
    case "tick" =>
      val now = System.currentTimeMillis

      queue.filter(a=>now - a._1 > tickTime ).map(_._2).sortBy(_.timestamp).foreach(sendMessageToActor)
      queue = queue.filterNot(a=>now - a._1 > tickTime )
    case setupProcessingListener: SetupProcessingListener =>
      processingListener = setupProcessingListener.processingListener
    case RegisterBrockerQueue(queueName) =>
      log.info(s"Creating queue for $queueName")
      val destination = session.createQueue(queueName)
      val consumer = session.createConsumer(destination)
      consumer.setMessageListener(messageListener(queueName))
      registeredNumber = registeredNumber + 1
    case ShutdownNotification() =>
      registeredNumber = registeredNumber - 1

      if (registeredNumber == 0) {
        log.info("The all registered brockers finished. Started procedure of preparing and sending the result.")

        implicit val timeout = Timeout(2 seconds)
        queue.map(_._2).sortBy(_.timestamp).foreach(sendMessageToActor)
        queue = List()
        log.info("Sending request for results to workers")
        val result: Set[LogicResultResponse] = Await.result(Future.sequence(processingLogicActors.values.map(_ ? GetResult)).map(_.asInstanceOf[Iterable[LogicResultResponse]]), 2 seconds).toSet

        log.info("Received result from workers")

        val transactions: java.util.Set[JavaTransaction] = result.foldLeft(List[Transaction]()) { case (acc, el) => el.logicResult.transactions ::: acc }.toSet.map((t: Transaction) => new JavaTransaction(t.id, t.details.amount, t.details.price, t.product, t.brokerBuy, t.brokerSell, t.clientBuy, t.clientSell))

        log.debug("merged transactions " + transactions)
        val ob: List[OrderBook] = {
          result.flatMap { r =>
            if (r.logicResult.buyOrders.isEmpty && r.logicResult.sellOrders.isEmpty) {
              Set[OrderBook]()
            } else {
              val orderBook = {
                new OrderBook(r.product,
                  r.logicResult.buyOrders.sortBy(r => (r.details.price, r.timestamp))(Ordering.Tuple2(Ordering.Int.reverse, Ordering.Long)).zipWithIndex.map(a => new OrderEntry(a._2 + 1, a._1.broker, a._1.details.amount, a._1.details.price, a._1.client)),
                  r.logicResult.sellOrders.sortBy(r => (r.details.price, r.timestamp))(Ordering.Tuple2(Ordering.Int, Ordering.Long)).zipWithIndex.map(a => new OrderEntry(a._2 + 1, a._1.broker, a._1.details.amount, a._1.details.price, a._1.client)))
              }
              Set[OrderBook](orderBook)
            }
          }.toList
        }
        log.debug("merged orderbooks " + ob)
        processingListener.processingDone(new SolutionResult(transactions, Set[OrderBook](ob: _*)))
        self ! PoisonPill
      }
    case orderMessage: WithTimestamp =>
      log.debug(s"added message [$orderMessage] to the queue")
      queue = (System.currentTimeMillis ,orderMessage) :: queue
  }

  def sendMessageToActor(message: WithTimestamp) = message match {
    case message: WithProduct =>
      log.debug("Received request of create or sell order")
      val actor = if (processingLogicActors.containsKey(message.product)) {
        log.debug(s"Actor for ${message.product} exists. Getting the existing actor.")
        processingLogicActors.get(message.product).get
      } else {
        log.debug(s"Actor for ${message.product} doesn't exist. Creating a new actor.")
        val actorOf: ActorRef = context.actorOf(LogicResultActor.props(message.product))
        processingLogicActors = processingLogicActors + (message.product -> actorOf)
        actorOf
      }
      actor ! message

    case message: OrderMessageType =>
      log.debug(s"Broadcasting the message $message to all actors.")
      processingLogicActors.values.foreach(_ ! message)
    case any =>
      log.error("Unhandled message: " + any)
  }

  def messageListener(queueName: String): MessageListener = { (message: Message) =>
    val text: String = message.asInstanceOf[TextMessage].getText
    mapJsonToProperMessage(text).foreach(_.apply())
  }

   override val jsonToProperMessage: ConvertAndRun =
      convertAndRun[CreateSellOrder](sendToSelf) _ ::
        convertAndRun[CreateBuyOrder](sendToSelf) _ ::
        convertAndRun[ModificateOrder](sendToSelf) _ ::
        convertAndRun[CancelOrder](sendToSelf) _ ::
        convertAndRun[ShutdownNotification](sendToSelf) _ ::
        Nil


  def sendToSelf[A](value: A): Unit = {
    self ! value
  }
}

object StockExchangeActor {

  case class SetupProcessingListener(processingListener: ProcessingListener)

  case class RegisterBrockerQueue(queueName: String)

}
