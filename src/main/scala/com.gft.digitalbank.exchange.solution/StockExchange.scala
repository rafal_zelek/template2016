package com.gft.digitalbank.exchange.solution

import java.util

import akka.actor.Actor.Receive
import akka.actor.SupervisorStrategy.{Decider, Restart, Stop}
import akka.actor.{Actor, ActorInitializationException, ActorKilledException, ActorRef, ActorSystem, AllForOneStrategy, OneForOneStrategy, Props, SupervisorStrategy, Terminated}
import com.gft.digitalbank.exchange.Exchange
import com.gft.digitalbank.exchange.listener.ProcessingListener
import com.gft.digitalbank.exchange.model.{OrderBook, SolutionResult, Transaction}
import com.gft.digitalbank.exchange.solution.StockExchangeActor.{RegisterBrockerQueue, SetupProcessingListener}

import scala.collection.JavaConversions._


class StockExchange extends Exchange {
  val system = ActorSystem("System")

  val stockExchangeActor = system.actorOf(Props[StockExchangeActor], name = "Supervisor")

  override def setDestinations(list: util.List[String]): Unit = {
    list.foreach(stockExchangeActor ! RegisterBrockerQueue(_))
  }

  override def register(processingListener: ProcessingListener): Unit = {
    stockExchangeActor ! SetupProcessingListener(processingListener)
  }

  override def start(): Unit = {
  }
}
