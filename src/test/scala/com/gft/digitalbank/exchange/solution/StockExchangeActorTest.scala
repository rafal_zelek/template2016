package com.gft.digitalbank.exchange.solution

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import org.scalatest.{FunSuiteLike, Matchers}

class StockExchangeActorTest extends TestKit(ActorSystem("StockExchangeActorTest", com.typesafe.config.ConfigFactory.load())) with FunSuiteLike with Matchers {
  test("initial test") {
    val stockExchangeTestActor = system.actorOf(Props(new StockExchangeActor()), "StockExchangeTestActor")
    stockExchangeTestActor ! "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    Thread.sleep(5000)
  }
}
