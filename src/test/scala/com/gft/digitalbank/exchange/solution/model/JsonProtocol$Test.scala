package com.gft.digitalbank.exchange.solution.model

import com.gft.digitalbank.exchange.solution.model.JsonProtocol._
import org.scalatest.{FunSuite, Matchers}
import spray.json.JsonParser


class JsonProtocol$Test extends FunSuite with Matchers with JsonProtocolTestExamples {
  test("parsing ModificateOrder example") {
    val expected = ModificateOrder(3, 3L, "b001", 1, Details(10, 1))
    JsonParser(modificateOrderJson).convertTo[ModificateOrder] shouldEqual expected
  }

  test("parsing CancelOrder example") {
    val expected = CancelOrder(4, 4L, "b001", 2)
    JsonParser(cancelOrderJson).convertTo[CancelOrder] shouldEqual expected
  }

  test("parsing shutdownNotification example") {
    JsonParser(shutdownNotificationJson).convertTo[ShutdownNotification] shouldEqual ShutdownNotification()
  }

  test("parsing CreateSellOrder example") {
    val expected = CreateSellOrder(2, 2L, "b001", "c002", "xyz", Details(15, 10))
    JsonParser(createSellOrderJson).convertTo[CreateSellOrder] shouldEqual expected

  }
  test("parsing CreateBuyOrder example") {
    val expected = CreateBuyOrder(1, 1L, "b001", "c001", "abc", Details(10, 1))
    JsonParser(createBuyOrderJson).convertTo[CreateBuyOrder] shouldEqual expected
  }
}

