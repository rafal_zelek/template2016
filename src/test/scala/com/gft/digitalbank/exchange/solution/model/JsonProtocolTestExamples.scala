package com.gft.digitalbank.exchange.solution.model


trait JsonProtocolTestExamples {

  val modificateOrderJson: String =
    """{
                   "messageType": "modification",
                   "id": 3,
                   "timestamp": 3,
                   "broker": "b001",
                   "modifiedOrderId": 1,
                   "details": {
                     "amount": 10,
                     "price": 1.25
                   }
                 }"""

  val cancelOrderJson =
    """{
                   "messageType": "cancel",
                   "id": 4,
                   "timestamp": 4,
                   "broker": "b001",
                   "cancelledOrderId": 2
                 }"""

  val shutdownNotificationJson =
    """{
                   "messageType": "shutdownNotification"
                 }"""

  val createSellOrderJson =
    """
      |{
      |  "messageType": "order",
      |  "side": "SELL",
      |  "id": 2,
      |  "timestamp": 2,
      |  "broker": "b001",
      |  "client": "c002",
      |  "product": "xyz",
      |  "details": {
      |    "amount": 15,
      |    "price": 10.00
      |  }
      |}
    """.stripMargin


  val createBuyOrderJson =
    """
      |{
      |  "messageType": "order",
      |  "side": "BUY",
      |  "id": 1,
      |  "timestamp": 1,
      |  "broker": "b001",
      |  "client": "c001",
      |  "product": "abc",
      |  "details": {
      |    "amount": 10,
      |    "price": 1
      |  }
      |}
    """.stripMargin
}
