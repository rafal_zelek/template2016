package com.gft.digitalbank.exchange.solution.model

import akka.actor.{ActorLogging, ActorSystem}
import akka.testkit.{TestKit, TestProbe}
import org.scalatest.{FunSuiteLike, Matchers}

import scala.concurrent.duration._
import JsonProtocol._


class MessageToFunctionConverterTest extends TestKit(ActorSystem("MessageToFunctionConverterTest", com.typesafe.config.ConfigFactory.load()))
  with FunSuiteLike with Matchers with MessageToFunctionConverter with JsonProtocolTestExamples{

  val createSellOrderProbe = TestProbe()
  val createBuyOrderProbe = TestProbe()
  val modificateOrderProbe = TestProbe()
  val cancelOrderProbe = TestProbe()
  val shutdownNotificationProbe = TestProbe()

  val defaultTimeout = 100 milliseconds

  override val jsonToProperMessage: ConvertAndRun =
    convertAndRun[CreateSellOrder]({ value => createSellOrderProbe.ref ! value;println("createSellOrderProbe")}) _ ::
    convertAndRun[CreateBuyOrder]({value => createBuyOrderProbe.ref ! value;println("createBuyOrderProbe")}) _ ::
    convertAndRun[ModificateOrder]({value => modificateOrderProbe.ref ! value;println("modificateOrderProbe")}) _ ::
    convertAndRun[CancelOrder]({value => cancelOrderProbe.ref ! value;println("cancelOrderProbe")}) _ ::
    convertAndRun[ShutdownNotification]({value => shutdownNotificationProbe.ref ! value;println("shutdownNotificationProbe")}) _ ::
    Nil

  test("only CreateSellOrder should receive message") {
    mapJsonToProperMessage(createSellOrderJson).foreach(_.apply())
    createSellOrderProbe.expectMsgType[CreateSellOrder]
    createBuyOrderProbe.expectNoMsg(defaultTimeout)
    modificateOrderProbe.expectNoMsg(defaultTimeout)
    cancelOrderProbe.expectNoMsg(defaultTimeout)
    shutdownNotificationProbe.expectNoMsg(defaultTimeout)
  }
  test("only CreateBuyOrder should receive message") {
    mapJsonToProperMessage(createBuyOrderJson).foreach(_.apply())
    createSellOrderProbe.expectNoMsg(defaultTimeout)
    createBuyOrderProbe.expectMsgType[CreateBuyOrder]
    modificateOrderProbe.expectNoMsg(defaultTimeout)
    cancelOrderProbe.expectNoMsg(defaultTimeout)
    shutdownNotificationProbe.expectNoMsg(defaultTimeout)
  }
  test("only ModificateOrder should receive message") {
    mapJsonToProperMessage(modificateOrderJson).foreach(_.apply())
    createSellOrderProbe.expectNoMsg(defaultTimeout)
    createBuyOrderProbe.expectNoMsg(defaultTimeout)
    modificateOrderProbe.expectMsgType[ModificateOrder]
    cancelOrderProbe.expectNoMsg(defaultTimeout)
    shutdownNotificationProbe.expectNoMsg(defaultTimeout)
  }
  test("only CancelOrder should receive message") {
    mapJsonToProperMessage(cancelOrderJson).foreach(_.apply())
    createSellOrderProbe.expectNoMsg(defaultTimeout)
    createBuyOrderProbe.expectNoMsg(defaultTimeout)
    modificateOrderProbe.expectNoMsg(defaultTimeout)
    cancelOrderProbe.expectMsgType[CancelOrder]
    shutdownNotificationProbe.expectNoMsg(defaultTimeout)
  }
  test("only ShutdownNotification should receive message") {
    mapJsonToProperMessage(shutdownNotificationJson).foreach(_.apply())
    createSellOrderProbe.expectNoMsg(defaultTimeout)
    createBuyOrderProbe.expectNoMsg(defaultTimeout)
    modificateOrderProbe.expectNoMsg(defaultTimeout)
    cancelOrderProbe.expectNoMsg(defaultTimeout)
    shutdownNotificationProbe.expectMsgType[ShutdownNotification]
  }
}
