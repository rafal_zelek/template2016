package com.gft.digitalbank.exchange.solution

import com.gft.digitalbank.exchange.solution.Logic._
import com.gft.digitalbank.exchange.solution.model._
import org.scalatest.{FunSuite, GivenWhenThen, Matchers}

class GeneralLogicTest extends FunSuite with Matchers with GivenWhenThen {
  val emptyLogicResult = LogicResult(List(), List(), List())

  test("adding createSellOrder to an empty list") {
    val createSellOrder = CreateSellOrder(1, 1L, "1", "100", "A", Details(100, 100))
    val result = processReceivedMessage(createSellOrder, emptyLogicResult)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.sellOrders should contain theSameElementsInOrderAs List(createSellOrder)
  }
  test("adding modificateOrder which change the order amount to a list which contains one element") {
    val orders = emptyLogicResult.copy(sellOrders = List(CreateSellOrder(1, 1L, "1", "100", "A", Details(100, 100))))
    val modificateOrder = ModificateOrder(2, 1L, "1", 1, Details(200, 100))
    val result = processReceivedMessage(modificateOrder, orders)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.sellOrders should contain theSameElementsInOrderAs List(CreateSellOrder(1, 1L, "1", "100", "A", Details(200, 100)))
  }
  test("adding createBuyOrder to an empty list") {
    val orders = emptyLogicResult
    val createBuyOrder = CreateBuyOrder(1, 1L, "1", "100", "A", Details(100, 100))
    val result = processReceivedMessage(createBuyOrder, orders)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.buyOrders should contain theSameElementsInOrderAs List(createBuyOrder)
  }
  test("adding createBuyOrder asdasd") {
    val orders = emptyLogicResult.copy(sellOrders = List(CreateSellOrder(1, 1L, "1", "100", "A", Details(100, 100))))
    val createBuyOrder = CreateBuyOrder(2, 2L, "2", "101", "A", Details(50, 100))
    val result = processReceivedMessage(createBuyOrder, orders)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.buyOrders should contain theSameElementsInOrderAs List(createBuyOrder)
  }

  test("the logic should ") {
    val cancelledOrderId: Int = 2
    val orders = emptyLogicResult.copy(buyOrders = List(CreateBuyOrder(1, 1L, "1", "100", "A", Details(100, 100))),
      sellOrders = List(CreateSellOrder(cancelledOrderId, 2L, "2", "101", "A", Details(100, 100))))

    val result = processReceivedMessage(CancelOrder(3, 3L, "2", cancelledOrderId), orders)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.buyOrders should contain theSameElementsInOrderAs List(CreateBuyOrder(1, 1L, "1", "100", "A", Details(100, 100)))
    result.sellOrders shouldBe empty
  }
  test("the logic shouldssssssssssssss ") {
    val cancelledOrderId: Int = 2
    val orders = emptyLogicResult.copy(buyOrders = List(CreateBuyOrder(1, 1L, "1", "100", "A", Details(100, 100))),
      sellOrders = List(CreateSellOrder(cancelledOrderId, 2L, "2", "101", "A", Details(100, 100))))

    val result = processTransactions(orders)
    println("the result is:")
    println("==============")
    println(result)
    println("==============")
    result.buyOrders should contain theSameElementsInOrderAs List(CreateBuyOrder(1, 1L, "1", "100", "A", Details(100, 100)))
    result.sellOrders shouldBe empty
  }
}
